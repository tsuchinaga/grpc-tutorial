package main

import (
	"context"
	"fmt"
	pb "gitlab.com/tsuchinaga/grpc-tutorial/proto"
	"google.golang.org/grpc"
	"io"
	"log"
)

const (
	address = "localhost:5432"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGreeterClient(conn)

	// Contact the server and print out its response.
	ctx := context.Background()

	var name string
	for {
		fmt.Println("なまえいれーや")
		_, _ = fmt.Scan(&name)
		r, err := c.SayHello(ctx, &pb.HelloRequest{Name: name})
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		log.Printf("Greeting: %s", r.Message)

		r, err = c.SayHelloAgain(ctx, &pb.HelloRequest{Name: name})
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		log.Printf("Greeting: %s", r.Message)

		stream, err := c.PleaseHello(ctx, &pb.HelloRequest{Name: name})
		if err != nil {
			log.Println(err)
			continue
		}
		for {
			r, err := stream.Recv()
			if err != nil {
				if err != io.EOF {
					log.Println(err)
				}
				break
			}
			log.Printf("Greeting: %s", r.Message)
		}
	}
}
